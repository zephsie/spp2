﻿namespace Tests.dto
{
    public class Inner
    {
        public int Int { get; set; }

        public override string ToString() =>
            $"""
            Type: {GetType()}:
            int: {Int}
            """;
    }
}
