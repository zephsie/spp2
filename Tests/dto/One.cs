﻿namespace Tests.dto
{
    public class One
    {
        public int Id;

        public CyclicOne? CyclicOne;

        public override string ToString() =>
            $"""
            Type: {GetType}
            Id: {Id}
            B: {CyclicOne}
            """;
    }
}
