﻿namespace Tests.dto
{
    public class CuclicTwo
    {
        public int CId;
        public CyclicOne? CyclicTwo;

        public override string ToString() =>
            $"""
            Type: {GetType()}
            Id: {CId}
            B: {CyclicTwo}
            """;
    }
}
