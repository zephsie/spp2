﻿namespace Tests.dto
{
    public class CyclicOne(int id)
    {
        public readonly int Id = id;

        public CuclicTwo? CyclicTwo;

        public override string ToString() =>
            $"""
            Type: {GetType()} 
            Id: {Id} 
            C: {CyclicTwo}
            """;
    }
}
