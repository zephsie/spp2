﻿namespace Tests.dto
{
    public class Outer
    {
        public string? String { get; set; }
        public int Int { get; set; }
        public float Float { get; set; }
        public double Double;
        public decimal Decimal;
        public Inner? Inner;
        public IEnumerable<int>? Enumerable;
        public List<string>? List;
        public DateTime DateTime { get; set; }
        public Uri? Uri { get; set; }

        public Outer(double dbl, Inner testDTO)
        {
            Double = dbl;
            Inner = testDTO;
        }

        public override string ToString() =>
            $"""
            Type: {GetType()}
            string: {String}
            int: {Int}
            float: {Float}
            double: {Double}
            decimal: {Decimal}
            enumerable: {string.Join(", ", Enumerable?.Select(item => $"{item}") ?? [])}
            list: {string.Join(", ", List?.Select(item => $"{item}") ?? [])}
            datetime: {DateTime}
            innerDTO: {Inner}
            uri: {Uri}
            """;
    }
}
