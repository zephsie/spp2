

using FakerLab.Faker;
using Tests.dto;
using Tests.generator;
using Xunit.Abstractions;

namespace Tests
{
    public class UnitTests
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private Faker _faker = new();
        private readonly FakerConfig _fakerConfig = new();

        public UnitTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void Test_CyclicalDependencies()
        {
            var res = _faker.Create<One>();

            Assert.NotNull(res);
            Assert.NotNull(res.CyclicOne);
            Assert.NotNull(res.CyclicOne.CyclicTwo);
            Assert.Null(res.CyclicOne.CyclicTwo.CyclicTwo);
        }
        

        [Fact]
        public void Test_NestedDTOs()
        {
            var res = _faker.Create<Outer>();

            Assert.NotNull(res);
            Assert.NotNull(res.Inner);
        }

        [Fact]
        public void Test_FakerConfig()
        {
            var generator = new Custom();
            _fakerConfig.Add<Inner, int, Custom>(dto => dto.Int);
            _faker = new Faker(_fakerConfig);

            var res = _faker.Create<Inner>();

            Assert.NotNull(res);
            Assert.Equal(generator.GetValue(), res.Int);
        }

        [Fact]
        public void Test_Enumerations()
        {
            var res = _faker.Create<Outer>();

            Assert.NotNull(res);
            Assert.NotNull(res.Enumerable);
        }
        

        [Fact]
        public void Test_SystemClassDateTime()
        {
            var res = _faker.Create<Outer>();

            Assert.NotEqual(default, res!.DateTime);
        }
        

        [Fact]
        public void Test_PrivateConstructor()
        {
            var res = _faker.Create<PrivateConstructor>();

            Assert.NotNull(res);
        }
        
        
        [Fact]
        public void Test()
        {
            var config = new FakerConfig();
            config.Add<Outer, int, Custom>(b => b.Int);

            var faker = new Faker(config);

            faker.AddGeneratorWithPlugin(@"C:\Users\alameda\RiderProjects\MPP\lab2\BoolGenerator\bin\Debug\net8.0\BoolGenerator.dll");
            faker.AddGeneratorWithPlugin(@"C:\Users\alameda\RiderProjects\MPP\lab2\UriGenerator\bin\Debug\net8.0\UriGenerator.dll");

            _testOutputHelper.WriteLine(faker.Create<Outer>()?.ToString());
        }
    }
}