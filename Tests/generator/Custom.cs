﻿using FakerLab.Generators;

namespace Tests.generator
{
    public class Custom : IGenerator<int>
    {
        public int GetValue() => 42;
    }
}
