﻿namespace FakerLab.Generators.CollectionGenerators;

public class GeneratorIEnumerable<T, TGenerator> : IGenerator<IEnumerable<T>> where TGenerator : IGenerator<T>, new()
{
    private readonly Random _random = new();
    private readonly TGenerator _generator = new();

    public IEnumerable<T> GetValue()
    {
        var capacity = _random.Next(16);
        var instance = new List<T>(_random.Next(capacity));

        for (var i = 0; i < capacity; i++)
        {
            instance.Add(_generator.GetValue());
        }

        return instance;
    }
}