﻿namespace FakerLab.Generators.CollectionGenerators;

public class GeneratorList<T, TGenerator> : IGenerator<List<T>> where TGenerator : IGenerator<T>, new()
{
    private readonly Random _random = new();
    private readonly TGenerator _generator = new();

    public List<T> GetValue()
    {
        var capacity = _random.Next(16);
        var instance = new List<T>(_random.Next(capacity));

        for (var i = 0; i < capacity; i++)
        {
            instance.Add(_generator.GetValue());
        }

        return instance;
    }
}