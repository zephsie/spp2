﻿using FakerLab.Generators.NumericGenerators.Integers;

namespace FakerLab.Generators.TimeGenerators;

public class GeneratorTimeSpan : IGenerator<TimeSpan>
{
    private readonly GeneratorInt _intGenerator = new();
    
    public TimeSpan GetValue()
    {
        var days = _intGenerator.GetValue();
        var hours = _intGenerator.GetValue() % 24 + 1;
        var minutes = _intGenerator.GetValue() % 60 + 1;
        var seconds = _intGenerator.GetValue() % 60 + 1;
        var milliseconds = _intGenerator.GetValue() % 1000 + 1;
        var microseconds = _intGenerator.GetValue() % 1000 + 1;

        return new TimeSpan(days, hours, minutes, seconds, milliseconds, microseconds);
    }
}