﻿using FakerLab.Generators.NumericGenerators.Integers;

namespace FakerLab.Generators.TimeGenerators;

public class GeneratorDateTime : IGenerator<DateTime>
{
    private readonly GeneratorInt _intGenerator = new();
    
    public DateTime GetValue()
    {
        var year = _intGenerator.GetValue() % 10000;
        var month = _intGenerator.GetValue() % 12 + 1;  
        var day = _intGenerator.GetValue() % 28 + 1;
        var hour = _intGenerator.GetValue() % 24 + 1;
        var minute = _intGenerator.GetValue() % 60 + 1;
        var second = _intGenerator.GetValue() % 60 + 1;
        var millisecond = _intGenerator.GetValue() % 1000 + 1;
        var microsecond = _intGenerator.GetValue() % 1000 + 1;

        return new DateTime(year, month, day, hour, minute, second, millisecond, microsecond, DateTimeKind.Utc);
    }
}