﻿using System.Text;

namespace FakerLab.Generators.StringGenerators;

public class GeneratorString : IGenerator<string>
{
    private readonly Random _random = new();
    private readonly string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public string GetValue()
    {
        const int maxLen = 16;
        var length = _random.Next(maxLen);

        var stringBuilder = new StringBuilder(length);

        for (var i = 0; i < length; i++)
        {
            var index = _random.Next(_chars.Length);
            var randomChar = _chars[index];
            stringBuilder.Append(randomChar);
        }

        return stringBuilder.ToString();
    }
}