﻿using FakerLab.Generators;
using FakerLab.Generators.StringGenerators;

namespace UriGenerator;


public class GeneratorUri : IGenerator<Uri>
{
    private readonly GeneratorString _generator = new();
    
    public Uri GetValue()
    {
        var builder = new UriBuilder
        {
            Scheme = "http",
            Host = "localhost",
            Path = _generator.GetValue()
        };

        return builder.Uri;
    }
}